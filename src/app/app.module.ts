import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { CountryDetailComponent } from './components/country-detail/country-detail.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import {CountryService} from './service/country.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CountryDetailComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyAXVn3Y9Aau9-JV65V7WC2UIOG-X8R1Vbk'
    })
  ],
  providers: [CountryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
