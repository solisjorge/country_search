import { Injectable, Output, EventEmitter } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
	//This base url could be in an environment or settings file
	//hardcoded here for simplicity
	private domain:string = 'https://restcountries.eu/rest/v2/'
  @Output()
  public loading:EventEmitter<string>;

  constructor(private _http:HttpClient) {
    this.loading = new EventEmitter<string>();
  }

  //get the full list, since there are only 254 countries in the full list
  getAllCountries():Observable<any>{
     this.start()
  	 return this._http.get(this.domain + 'all').pipe(
      finalize(() => this.end()),
      catchError(this.handleError)
     ); 
  }

  handleError(error: any) {
    console.log(error);
    return Observable.throw(error || 'Server error');
  }

  //helper functions to show/hide preloader
  start(){
     this.loading.emit('start');
  }
  end(){
    this.loading.emit('stop')
  }
}
