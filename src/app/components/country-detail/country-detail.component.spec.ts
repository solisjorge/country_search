import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {NO_ERRORS_SCHEMA } from '@angular/core';
import {NavbarComponent} from '../shared/navbar/navbar.component';
import { CountryDetailComponent } from './country-detail.component';
import { AgmCoreModule, MapsAPILoader  } from '@agm/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CountryModel} from '../../model/country.model';
import {CountryMockModel} from '../../model/countrymock.model';
import { ActivatedRoute,convertToParamMap } from '@angular/router';

describe('CountryDetailComponent', () => {
  let component: CountryDetailComponent;
  let fixture: ComponentFixture<CountryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryDetailComponent,NavbarComponent ],
      imports: [AgmCoreModule.forRoot(),RouterTestingModule],
      providers:[{provide:CountryModel, useClass:CountryMockModel}, 
                {provide: ActivatedRoute, useValue: {snapshot: {
                  paramMap: convertToParamMap({name: 'ARG'})}}}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
