import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {CountryModel} from '../../model/country.model';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.css']
})
export class CountryDetailComponent implements OnInit {
  public country: any ={};
  public title:string = '';
  public zoom:number = 5; //generic zoom that match most countries
  constructor(private route: ActivatedRoute, 
              private _countryModel:CountryModel) { }

  ngOnInit() {
    //this updates the list in the model in case of reload
    var list = this._countryModel.list 
    //get selected country from url
    var id = this.route.snapshot.paramMap.get('name');
    this.country = this._countryModel.getCountry(id)
    this.title = this.country.name;
    //TODO: use google maps api to get country lat/long bounds and adjust zoom
  }
}
