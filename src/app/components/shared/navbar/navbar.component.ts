import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  inputs: ['title']
})
export class NavbarComponent  {
  public title;

  constructor(private route: ActivatedRoute) { }


  //Function to show or hide back label
  isHome(){
      return window.location.href.indexOf('home') > -1 ? true : false;
  }

  

}
