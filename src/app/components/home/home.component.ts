import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormControl} from '@angular/forms';
import { debounceTime } from 'rxjs/operators'
import { Router } from '@angular/router';
import {CountryService} from '../../service/country.service';
import {Country} from '../../model/dto';
import {CountryModel} from '../../model/country.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public countriesList: any = [];
  public keyword:string;
  searchControl = new FormControl();

  constructor(private _countryService: CountryService, 
              private _countryModel:CountryModel, 
              public router: Router) { }

  ngOnInit() {
    //load the full list if is not in the model, so we don't need to call
    //the service on searching, there are 249 countries in total, that's 50kb!
    if(!this._countryModel.list.length)
      this.cargarPaises()
    
    this.searchControl.valueChanges.pipe(debounceTime(1000)).subscribe(value=>{
          this.countriesList = this._countryModel.search(value);
          //no match, show the no match in the list
          if(this.countriesList.length<1){
            var c:Country = Country.getInstance();
            c.name = "No match !";
            this.countriesList = [c];
          }
    })
    //every 10 minutes the model should be refreshed
    this._countryModel.propagar.subscribe(event => this.cargarPaises())
  }

  cargarPaises(){
    this._countryService.getAllCountries().subscribe(result=>this.onCountries(result))
  }

  onCountries(data:Country[]){
    this._countryModel.clearLocal(); //clear cache
    var total = data.length;
    for (var i = 0; i < total; i++) {
      this._countryModel.addCountry(data[i] as Country);
    }
    //save the list locally since this information will rarely change
    //this allow off-line browsing. The model will empty every 10 minutes
    //and emit a reload signal. If this component is alive will trigger 
    //a service load, otherwise will load the data in the onInit hook 
    this._countryModel.saveLocal();
  }

  //Navigate to country-detail view
  goToCountryDetail(country:Country){
    if(country.name == 'No match !') return;
    //store selection to the model
    this._countryModel.selected = country;
    //use id as identifier
    this.router.navigate(['country/'+ country.alpha3Code]);
  }
}
