import { async, ComponentFixture, TestBed, tick, fakeAsync, inject } from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NavbarComponent} from '../shared/navbar/navbar.component';
import { HomeComponent } from './home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';
import {CountryModel} from '../../model/country.model';
import {CountryMockModel} from '../../model/countrymock.model';
import {CountryService} from '../../service/country.service';
import {CountryMockService} from '../../service/countrymock.service';
import {Country} from '../../model/dto';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let country:Country = Country.getInstance();
  country.name = "Spain";

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent, NavbarComponent ],
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule,RouterTestingModule],
      providers: [{provide:CountryModel, useClass:CountryMockModel},
                  {provide:CountryService, useClass:CountryMockService} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    component.cargarPaises(); //just for cc
  });

   it('Search a country that doesnt exist', fakeAsync(() => { (1)
      component.searchControl.setValue('xr');
      fixture.detectChanges();
      tick(1050); (2)
      expect(component.countriesList[0].name).toBe('No match !');
    }));

   it('Load all countries',
        fakeAsync(inject([CountryMockService], (countryService) => {
          countryService.getAllCountries().subscribe((data) => {
            component.onCountries(data);
            component.searchControl.setValue('a');
            fixture.detectChanges();
            tick(1050); //debounce time
            expect(component.countriesList.length).toBe(3)
            expect(component.countriesList[0].name).toBe('Afghanistan');
          });
        })
    ));

   
     it('Go to detail', inject([Router], (router: Router) => {
      spyOn(router, 'navigate').and.stub();
      component.goToCountryDetail(country)
      expect(router.navigate).toHaveBeenCalledWith(['country/'+ country.alpha3Code]);
    }));
});
