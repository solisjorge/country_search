import { Component } from '@angular/core';
import {CountryService} from './service/country.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'Search countries';
  public loading:boolean = false;

  constructor(private _service:CountryService){
  	this._service.loading.subscribe(evt => this.showPreload(evt))
  }

  //preload is outside angular zone to avoid
  //dirty checking errors
  showPreload(evt){
  	switch (evt) {
  		case "start":
  			window["launchLoader"]();
  			break;
  		
  		case "stop":
  			window["stopLoader"]();
  			break;
  	}
  }
}
