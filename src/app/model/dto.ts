/**
* Definition of all of the entities
* in the application. This Data Transfer Objects
* usually mimic the database structure
*/
export class Country{
	constructor( public name:string,
				public topLevelDomain:string[],
				public alpha2Code:string,
				public alpha3Code:string,
				public callingCodes:any[],
				public capital:string,
				public altSpellings:string[],
				public region:string,
				public subregion:string,
				public population:number,
				public latlng:number[],
				public demonym:string,
				public area:number,
				public gini:number,
				public timezones:string[],
				public borders:string[],
				public nativeName:string,
				public numericCode:string,
				public currencies:Currency[],
				public languages:Language[],
				public translations:Translation,
				public flag:string,
				public regionalBlocs:Block[],
				public cioc:string
	){}
	static getInstance():Country{
		return new Country('',[''],'','',[],'',[],'','',0,[],'',0,0,[],[],'','',[],[],null,'',[],'');
	}
}

export class Currency{
	constructor( public code:string,
				public name:string,
				public symbol:string
	){}
}

export class Language{
	constructor( public iso639_1:string,
				public iso639_2:string,
				public name:string,
				public nativeName:string
		){}
}

export class Translation{
	constructor(  public de:string,
            public es:string,
            public fr:string,
            public ja:string,
            public it:string,
            public br:string,
            public pt:string,
            public nl:string,
            public hr:string,
            public fa:string
		){}
}
export class Block{
	constructor(  
			public acronym:string,
            public name:string,
            public otherAcronyms:string[],
            public otherNames:string[]
           
		){}
}