import { Injectable,EventEmitter,Output } from '@angular/core';
import { Country } from './dto';

@Injectable({
  providedIn: 'root'
})
export class CountryModel {
	protected _list:Country[];
	protected _selected:Country;
	protected renewData:number = 600000; //10 minutes in miliseconds, use 0 for none
	@Output()
	public propagar:EventEmitter<string>;
	

	constructor() {
		this._list = [];
		this.propagar = new EventEmitter<string>();		
	}


	get list():Country[]{
		if(this.total<1){
			//get from local storage;
			var tmp = JSON.parse(sessionStorage.getItem('countryList'));
			if(tmp !== null) this._list = tmp;
		}
		return this._list;
	}

	get total():number{
	  return this._list.length;
	}

	set selected(sel:Country){
		this._selected = sel;
	}

	get selected():Country{
		return this._selected;
	}

	getEventEmitter():EventEmitter<string>{
		return this.propagar;
	}

	/**
	* Persist the list to the local storage
	* allowing to work off-line if neccesary
	**/
	saveLocal(){
		sessionStorage.setItem('countryList', JSON.stringify(this._list));
		//if renewData > 0, fire timeout
		if(this.renewData>0)
			setTimeout( () => this.reloadData(), this.renewData)
	}

	reloadData(){
		this.propagar.emit('Reload');
	}

	//Clear the local storage
	clearLocal(){
		sessionStorage.setItem('countryList', null)
		this._list = [];
	}

	addCountry(country:Country){
		var exist = this.getCountry(country.alpha3Code)
		if(!exist) this._list.push(country);
	}
	
	getCountry(code:string):Country{
		//alpha3Code is unique
		return this._list.filter(item => item.alpha3Code.toLowerCase() == code.toLowerCase())[0];
	}

	search(name:string){
		return this._list.filter(item => item.name.toLowerCase().search(name.toLowerCase()) != -1);
	}
	
	getSortedList(){
		this._list.sort(this.compare);
		return this._list;
	}

	compare(a,b) {
	  if (a.name < b.name)
	    return -1;
	  if (a.name > b.name)
	    return 1;
	  return 0;
	}
}