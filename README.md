# Search countries #

This is the test project for Angular devs at HCD

### Project Description ###

#### Goals
This small application allows to search countries by name and consult some information about them. Is extremely simple and the goal is to challenge developers to build the best version possible. The data source comes from the public API from [restcountries](https://restcountries.eu/). Below some screenshots to give an idea about what the application does

![Screenshots](http://devarg3818.com/assets/searchCountry.png)

#### Architechture 
The application has visually only 2 screens: master search list and detail, although there's also a navigation bar on the top. So visually we have 3 components that are in the components folder
The data comes from a public API, so we have a service folder where we store the external services access class
The application is meant to work offline as well, so we have a model to store the data, that refresh itself every 10 minutes. The model and the data transfer object classes (that mimics API entities) are on the model folder. Below the folder structure of the project

![Screenshots](http://devarg3818.com/assets/structure.png)

#### Coding  
There are a few keypoints to highligth from our code that we want to spot in order to illustrate some of what we consider best practices, although this is always a matter open to discussion.

**Data load and service**

Considering the data needs of the application, we soon realize that we are working with  a list of countries that are in total only 249. Also looking at restcountries API, we realize that the full list call ( /all ) get a list with the same details as the individual call ( /spain i.e) So a desirable goal is to load the full list on the beginning in order to minimize the API usage (therefore bandwith consumption) After testing the call, we can surely know that the full list is about 50 KB, so is not really a big deal.
We use an Observable to return the call result, but also call a start and end methods that are used to show/hide a preloader to give the user feedback about data loading. See the excerpt below

	getAllCountries():Observable<any>{
	     this.start()
	  	 return this._http.get(this.domain + 'all').pipe(
	      finalize(() => this.end()),
	      catchError(this.handleError)
	     ); 
	}

**Debounce search**

Search trough a list performing a lookup is very common nowadays, but we need to give some space to the user to finish typing before digging the list. We use the RXJS debounce operator to wait at least 1 second after the user has finished. Also if there's no match, we populate the list with an empty entry to give feedback to the user.

	 this.searchControl.valueChanges.pipe(debounceTime(1000)).subscribe(value=>{
	          this.countriesList = this._countryModel.search(value);
	          //no match, show the no match in the list
	          if(this.countriesList.length<1){
	            var c:Country = Country.getInstance();
	            c.name = "No match !";
	            this.countriesList = [c];
	          }
	    })

![Screenshots](http://devarg3818.com/assets/debounce.png)

**Model usage**

Something important about using a framework as Angular is the option of managing state and data in the client side. The detail make usage of the model (injected) to get the country details and show properly. No extra loading is needed, everything sits in the model.

	 constructor(private route: ActivatedRoute, 
	              private _countryModel:CountryModel) { }

	  ngOnInit() {
	    var list = this._countryModel.list 
	    //get selected coutry from url
	    var id = this.route.snapshot.paramMap.get('name');
	    this.country = this._countryModel.getCountry(id)
	    this.title = this.country.name;
	  }

**Google Maps**

We use agm-map component since our maps needs are very simple. But there's always room for improvement: whe showing the map we use an average zoom of 5, that seems to work fine for most of the countries. But what about the big ones like USA, Russia or Australia? In those cases the map is cropped somewhere. Using the component we hit a limit here, because for an accurate map zoom, we really need to use google maps API to query the boundaries of the object (formally a rectangle of coordinates) to zoom to. Something for the TODO list !

	<agm-map [latitude]="country.latlng[0]" [longitude]="country.latlng[1]" [zoom]="zoom">
	</agm-map>

![Screenshots](http://devarg3818.com/assets/croppedMap.png)

**Emitter and subscription**

The Observer pattern is widely adopted in Angular, and we should use events and EventEmitter when we want to react to specific events. An example of this is the data refreshing after 10 minutes. In order to do that there are a couple of places to perform the actin: the service or the model (or both, why not?) We choose the model in a specific action: when it persists the data to the sessionStorage, we fire a timeout to reload the data. But the model is not in charge of dealing with external sources, so what we do is to emit an event that is handled by the main component to actually load the data

In the model:

	reloadData(){
		this.propagar.emit('Reload');
	} 

In home component

	this._countryModel.propagar.subscribe(event => this.cargarPaises())

**Unit testing**

Unit testing is a matter sometimes undervalued, but in fact there are coding methodologies like TDD (Test Driven Development) that are based on testing (though not always unit testing) Angular comes with a handy setup for Unit testing based on Karma test runner and Jasmine JS framework. If you use CLI to create the barebones of your classes, it automatically creates a `spec.ts` file that is included automatically in the test suite that you can run simple by invoking `ng test`
There are quite a few topics to cover about unit testing, and we agreed that probably is unnecessary for such a simple application, but on the other side is a good option to start writing your own tests. You can see and run a few test on the components on this application

![Screenshots](http://devarg3818.com/assets/unitTest.png)

### What is this repository for? ###

* Search country project full developed with unit testing
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Fork this project
* If you don't have Node and npm, please install from [here](https://www.npmjs.com/get-npm)
* Run npm install for the full Angular project and its dependencies
* There's no need of something else special, datasource come from public API
of  [restcountries] (https://restcountries.eu/)

### Who do I talk to? ###

* Jorge Solis at jorge.solis@hcd.es